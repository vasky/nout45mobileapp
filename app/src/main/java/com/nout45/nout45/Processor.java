package com.nout45.nout45;

public class Processor {
    public String name;
    public int photo;

    public static Processor[] items = {
            new Processor("43HAX4DM AMD Sempron", R.drawable.processor),
            new Processor("intel t4500 slgzc", R.drawable.processor),
            new Processor("HMN950DCR42GM", R.drawable.processor),
            new Processor("intel i3-2370 sr0dp", R.drawable.processor),
            new Processor("Intel t2390 для ноутбука sla4h", R.drawable.processor),
            new Processor("Процессор для ноутбука LF80537 T5600 intel", R.drawable.processor),
            new Processor("intel t6570 slgll", R.drawable.processor)
    };

    public Processor(String name, int photo) {

        this.name = name;
        this.photo = photo;
    }

    public String getName(){
        return this.name;
    }

    public int getRes(){
        return this.photo;
    }
}
