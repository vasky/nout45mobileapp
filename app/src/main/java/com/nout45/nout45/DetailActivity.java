package com.nout45.nout45;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    public static final String EXTRA_WORKOUT_ID = "id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        int itemId = (int) getIntent().getExtras().get(EXTRA_WORKOUT_ID);
        String name = ItemMainMenu.items[itemId].getName();

        TextView tv = findViewById(R.id.tv);
        tv.setText(name);
    }


}
