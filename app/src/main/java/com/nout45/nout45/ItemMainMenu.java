package com.nout45.nout45;

public class ItemMainMenu {

    public String name;

    public static ItemMainMenu[] items = {
            new ItemMainMenu("Проверить статус заказа"),
            new ItemMainMenu("Контакты"),
            new ItemMainMenu("Каталог"),
            new ItemMainMenu("Как работать с компьютером?"),
            new ItemMainMenu("Уход за техникой")
    };

    public ItemMainMenu(String name) {
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public String toString(){
        return this.name;
    }
}
