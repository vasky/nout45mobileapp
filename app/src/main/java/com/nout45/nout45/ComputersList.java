package com.nout45.nout45;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ComputersList extends AppCompatActivity implements itemsMenuFragment.Listener{
    private static final String TAG = "myLogs";
    ArrayList<String> arrayList = new ArrayList<>();


    DatabaseReference reff ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_computers_list);


        ListView lv = (ListView) findViewById(R.id.listFromInet);
//

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,arrayList);
        
        reff = FirebaseDatabase.getInstance().getReference().child("computers");
        reff.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                "{pavilionc45={img=res, name=pavilionc45, description=text}, php={img=img, name=php, description=desc}}";
                for(DataSnapshot ds : dataSnapshot.getChildren()){
                    arrayList.add(ds.getKey().toString());
                }


                ListView lv = (ListView) findViewById(R.id.listFromInet);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(ComputersList.this,android.R.layout.simple_list_item_1,arrayList);

                lv.setAdapter(adapter);

            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        lv.setAdapter(adapter);

        AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String computerName =  parent.getAdapter().getItem(position).toString();
                Intent intentGoodDetail = new Intent(ComputersList.this, GoodDetails.class);
                intentGoodDetail.putExtra(GoodDetails.EXTRA_GOOD_NAME, computerName);
                startActivity(intentGoodDetail);

            }
        };

        lv.setOnItemClickListener(itemClickListener);


    }


    @Override
    public void itemClicked(long id) {
        Intent intent = new Intent(this, ComputerDetail.class);
        intent.putExtra(ComputerDetail.EXTRA_WORKOUT_ID, (int)id);
        startActivity(intent);
    }
}
