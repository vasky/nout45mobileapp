package com.nout45.nout45;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class ContanctsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contancts);
    }

    public void call(View view){
        Intent intent = new Intent(Intent.ACTION_DIAL);
        String number = "89292269186";
        intent.setData(Uri.parse("tel:" + number));
        startActivity(intent);

    }

    public void sendMessageMail(View view){
        String mailto = "mailto:nout-45@mail.ru" +
                "?cc=" + "nout-45@mail.ru" ;

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse(mailto));

        startActivity(emailIntent);

    }
}
