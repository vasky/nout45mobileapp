package com.nout45.nout45;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

public class CategirysCatalog extends AppCompatActivity implements itemsMenuFragment.Listener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categirys_catalog);
    }

    @Override
    public void itemClicked(long id) {
        String nameActivity = categoryCatalog.items[(int)id].getName();

        switch (nameActivity) {
            case "Компьютеры":
                Intent intent = new Intent(this, ComputersList.class);
                startActivity(intent);
                break;
            case "Процессоры":
                Intent intent2 = new Intent(this, ProcessorsAll.class);
                startActivity(intent2);
                break;
        }

    }
}
