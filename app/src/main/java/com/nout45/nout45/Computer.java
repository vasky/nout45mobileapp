package com.nout45.nout45;

public class Computer {
    public String name;
    public int photo;
    public String description;

    public static Computer[] items = {
            new Computer("Pavilion 4404 hp", R.drawable.nout2, " Материнская плата: \n 1120710 Материнская плата ASROCK A320M-DVS R4.0, SocketAM4, AMD A320 \n\n Процессор:\n 499917 Процессор AMD A8 9600, SocketAM4 \n\nПроцессорный кулер:\n925437 \n\nУстройство охлаждения(кулер): \nDEEPCOOL ICE EDGE MINI FS V2.0 \n\nОперативная память:\n395780 \n\nМодуль памяти: \nCRUCIAL CT4G4DFS824A DDR4 — 4Гб 2400 \n\nДискретная видеокарта:\nне установлена \n\nЖесткий диск:\nне установлен \n\nОптический привод:\nне установлен \n\nКорпус:\n1169262 \n\nКорпус: \nmATX LINKWORLD VC-13M171, Micro-Tower, без БП, черный \n\nБлок питания:\n1049253 \n\nБлок питания: \nAEROCOOL VX PLUS, 350Вт \n\nОперационная система:\nне установлена \n\nГарантия:\n36 мес."),
            new Computer("Ноутбук Lenovo IdeaPad C340-15IWL черный", R.drawable.nout1, " Операционная система: \nWindows 10 Home \n\nЭкран:\nДиагональ экрана 15.6'' \n\nПроцессор:\nIntel Pentium \n\nОперативная память: \nРазмер оперативной памяти 4 ГБ \n\nГрафический ускоритель:\nМодель дискретной видеокарты - нет \n\nНакопители данных: \nОбщий объем жестких дисков (HDD) - нет \nОбщий объем твердотельных накопителей (SSD)"),
            new Computer("Ноутбук HP 17-by0174ur черный", R.drawable.nout2, " Операционная система : \nDOS \n\nДиагональ экрана:\n17.3'' \n\nЛинейка процессора :\nIntel Core i3 \n\nРазмер оперативной памяти : \n8 ГБ \n\nМодель дискретной видеокарты:\nнет \n\nОбщий объем твердотельных накопителей (SSD): \n128 ГБ "),
            new Computer("Ноутбук HP 15-bs157ur черный", R.drawable.nout3, " Операционная система: \nWindows 10 home \n\nДиагональ экрана:\n15.6'' \n\nЛинейка процессора:\nIntel Core i3 \n\nРазмер оперативной памяти: \n4ГБ \n\nМодель дискретной видеокарты:\nнет \n\nОбщий объем жестких дисков (HDD) : \n500ГБ"),
            new Computer("HP 14-dk0027ur серебристый", R.drawable.nout4, "Операционная система: \nWindows 10 home \n\nДиагональ экрана:\n14'' \n\nЛинейка процессора:\nAMD Ryzen 3 \n\nРазмер оперативной памяти: \n4 ГБ \n\nМодель дискретной видеокарты:\nнет \n\nОбщий объем твердотельных накопителей (SSD) : \n 256ГБ"),
    };

    public Computer(String name,int photo,String description) {

        this.name = name;
        this.photo = photo;
        this.description = description;
    }

    public String getName(){
        return this.name;
    }
    public String getDescription(){return this.description;}
    public int getRes(){
        return this.photo;
    }
}
