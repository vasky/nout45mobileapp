package com.nout45.nout45;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class AdviceCareDetail extends AppCompatActivity {

    public static final String EXTRA_WORKOUT_ID = "id";
    private int workoutId2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advice_care_detail);
    }

    @Override
    public void onStart() {
        super.onStart();

        TextView title = findViewById(R.id.titleAdviceCare);
        TextView description = findViewById(R.id.textView8);
        ImageView image = findViewById(R.id.imageView4);

        int workoutId = (int) getIntent().getExtras().get(EXTRA_WORKOUT_ID);
        AdviceCare workout = AdviceCare.items[(int) workoutId];
        title.setText(workout.getName());
        description.setText(workout.getDescription());
//        Log.i(":",Integer.parseInt( workout.getPhoto()));
        String text = "game_nout";
        image.setImageResource(workout.getRes() );
        setWorkout(workoutId);
    }
    public void setWorkout(int id) {
        this.workoutId2 = id;
    }
}
