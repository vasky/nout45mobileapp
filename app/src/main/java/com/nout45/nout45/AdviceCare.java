package com.nout45.nout45;

public class AdviceCare {
    public String name;
    public String description;
    public int photo;

    public static AdviceCare[] items = {
            new AdviceCare("Протирать экран дисплея", R.drawable.cleanpc, "не реже чем раз в одну неделю "+
                    "протирать экран дисплея, системный блок, клавиатуру и периферийные устройства от пыли."+
                    " При этом рекомендуется использовать мягкую тряпку и жидкости, предназначенные специально "+
                    "для ухода за соответствующими приборами. По мере использования компьютера в корпусе может накапливаться "+
                    "чрезмерное количество пыли, что в итоге может привести к созданию статического заряда и, как следствие, вывести" +
                    "элементы входящие в системный блок из строя. Немало пыли скапливается также в блоке питания, что также может быть причиной поломок."),
            new AdviceCare("Зазоры между вентиляционными отверстиями компьютера и стенами", R.drawable.airpcadvice,
                    "С целью обеспечения хорошей циркуляции воздуха крайне необходимо"+
                            " обеспечить достаточные зазоры между вентиляционными отверстиями компьютера и стенами помещения. И также"+
                            " нежелательно располагать системный блок вблизи штор, дабы избежать затягивания их вентилятором системы охлаждения компьютера."),
            new AdviceCare("Не ешьте за ПК",  R.drawable.eatpcadvice, " крайне нежелательно принимать пищу вблизи компьютера или над клавиатурой, поскольку попадание жидкости"+
                    "\n может являться причиной выхода из строя компьютера, а попадание внутрь крошек может вызвать механические повреждения."),
            new AdviceCare("Жесткий диск", R.drawable.harddriverpc,"Сам жесткий диск опломбирован, надежно установлен в корпусе и потому не нуждается в физическом уходе. Конечно, важна защита от трясок и перегрева, но эти вопросы решаются при сборке компьютера, а также с помощью правильной установки системного блока (об этом ниже). Непосредственно диски требуют периодической проверки и обслуживания программными средствами. Ежемесячного контроля для средне нагруженного компьютера будет вполне достаточно.\n" +
                    "Проверка диска на ошибки. Она уже обсуждалась выше, но тогда инициатором проверки была Windows. Иногда, даже если с компьютером все в порядке, нужно обслуживать диск самому. Запустите Scandisk для Windows (Мой компьютер -> Свойства диска -> Обслуживание -> Выполнить проверку или Пуск -> Программы -> Стандартные -> Служебные -> Проверка диска) и выберите «Полную проверку», которая включает не только контроль файловой системы, но и поиск физических ошибок чтения/записи на диск. Три-пять сбойных секторов на диске — не повод для паники, просто следует проверять диск чаще (еженедельно), а вот если их число растет каждую неделю, пора покупать новый винчестер.\n" +
                    "\n" +
                    "Дефрагментация диска. Эта операция является спутницей ежемесячной проверки на ошибки и выполняется после устранения всех ошибок. Суть ее проста — файловая система оптимизируется для наиболее быстрой и надежной работы. Оглавления каталогов записываются в начало диска, файлы из одного каталога перемещаются так, чтобы быть записанными по соседству и без фрагментации (файл целиком записывается в идущую подряд цепочку секторов на винчестере). Если пренебрегать дефрагментацией, работа ОС станет более медленной, а в случае сбоев будет очень трудно восстанавливать файлы на диске. Это не касается Windows 7, т.к. там реализовано это автоматически в фоновом режиме.\n" +
                    "\n" +
                    "Проверка на вирусы. Эта операция, конечно, не относится к обслуживанию дисков, но в качестве профилактической меры просто необходима, особенно в наш Информационный век. Конечно, для этого нужно иметь полноценную антивирусную программу с регулярно обновляемыми базами вирусов.\n" +
                    "\n" +
                    "Проверка срока жизни диска. Современные винчестеры обладают способностями самодиагностики."+
                    "\n Для использования этих возможностей был разработан S.M.A.R.T — индустриальный стандарт для контроля периферийных устройств в нормальном и сбойном режиме. Попросту"+
                    "\n говоря, винчестер сам регистрирует мелкие (неопасные) ошибки в работе и на основании этих данных прогнозирует, как скоро выйдет из строя. Чтобы извлечь эту информацию,"+
                    "\n воспользуйтесь специальной утилитой, например SiGuardian или HDDSpeed. Конечно, можно запускать их ежедневно, но в обычных случаях будет достаточно "+"" +
                    "\nеженедельной проверки. И если вдруг во время включения компьютер стал запрашивать нажатие на клавишу F1 то нужно бить тревогу, возможно это система диагностики S.M.A.R.T.")
    };

    public AdviceCare(String name, int photo, String description) {
        this.name = name;
        this.photo = photo;
        this.description = description;
    }

    public String getName(){
        return this.name;
    }

    public String getDescription(){return this.description;}
    public int getRes(){
        return this.photo;
    }
}
