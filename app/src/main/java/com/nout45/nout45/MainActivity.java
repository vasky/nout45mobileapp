package com.nout45.nout45;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements itemsMenuFragment.Listener {

    ArrayList<String> myArrayList = new ArrayList<>();
    ListView lv ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }


    @Override
    public void itemClicked(long id) {
        String nameActivity = ItemMainMenu.items[(int)id].getName();

        switch (nameActivity) {
            case "Контакты":
                Intent intent = new Intent(this, ContanctsActivity.class);
                intent.putExtra(DetailActivity.EXTRA_WORKOUT_ID, (int) id);
                startActivity(intent);
                break;
            case "Проверить статус заказа":
                Intent intent2 = new Intent(this, CheckStatus.class);
                startActivity(intent2);
                break;
            case "Каталог":
                Intent intent3 = new Intent(this, CategirysCatalog.class);
                startActivity(intent3);
                break;
            case "Как работать с компьютером?":
                Intent intent4 = new Intent(this, listUserAdvice.class);
                startActivity(intent4);
                break;
            case "Уход за техникой":
                Intent intent5 = new Intent(this, ListAdviceCare.class);
                startActivity(intent5);
                break;
        }

    }
}
