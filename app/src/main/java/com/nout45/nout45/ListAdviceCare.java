package com.nout45.nout45;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ListAdviceCare extends AppCompatActivity {

    private long workoutId;



    static interface Listener {
        void itemClicked(long id);
    };
    private listUserAdvice.Listener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_advice_care);

        String[] names = new String[AdviceCare.items.length];
        for (int i = 0; i < names.length; i++) {
            names[i] = AdviceCare.items[i].getName();
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_list_item_1,
                names);
        ListView lv = findViewById(R.id.listAdviceCare);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(ListAdviceCare.this, AdviceCareDetail.class);
                intent.putExtra(AdviceDetail.EXTRA_WORKOUT_ID, (int) id);
                startActivity(intent);
            }
        });
    }


}
