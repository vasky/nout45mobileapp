package com.nout45.nout45;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class GoodDetails extends AppCompatActivity {

    public static String EXTRA_GOOD_NAME = "name";
    DatabaseReference reff ;
    public TextView goodName ;
    public TextView goodDescription;
    public ImageView goodImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_good_details);

        goodName = findViewById(R.id.goodName);
        goodDescription = findViewById(R.id.goodDescription);
        goodImage = findViewById(R.id.goodImage);

        EXTRA_GOOD_NAME = getIntent().getExtras().get(EXTRA_GOOD_NAME).toString();

        reff = FirebaseDatabase.getInstance().getReference().child("computers");
        reff.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.d("myLogs", dataSnapshot.child(EXTRA_GOOD_NAME).getValue().toString());
                goodDescription.setText(dataSnapshot.child(EXTRA_GOOD_NAME).child("description").getValue().toString());
                goodName.setText(EXTRA_GOOD_NAME);
                Picasso.get().load(dataSnapshot.child(EXTRA_GOOD_NAME).child("img").getValue().toString()).into(goodImage);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
//        Log.d("myLogs", reff.toString());
    }
}
