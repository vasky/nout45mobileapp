package com.nout45.nout45;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class ProcessorDetail extends AppCompatActivity {

    public static final String EXTRA_WORKOUT_ID = "id";
    private int workoutId2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_processor_detail);
    }

    @Override
    public void onStart() {
        super.onStart();

        TextView title = findViewById(R.id.processorName);
        ImageView image = findViewById(R.id.imageOrderProcessor);

        int workoutId = (int) getIntent().getExtras().get(EXTRA_WORKOUT_ID);
        Processor workout = Processor.items[(int) workoutId];
        title.setText(workout.getName());
//        Log.i(":",Integer.parseInt( workout.getPhoto()));
        String text = "game_nout";
        image.setImageResource(workout.getRes() );
        setWorkout(workoutId);
    }
    public void setWorkout(int id) {
        this.workoutId2 = id;
    }
}
