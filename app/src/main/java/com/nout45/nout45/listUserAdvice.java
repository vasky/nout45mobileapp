package com.nout45.nout45;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class listUserAdvice extends AppCompatActivity {

    private long workoutId;



    static interface Listener {
        void itemClicked(long id);
    };
    private Listener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_user_advice);

        String[] names = new String[UseAdvice.items.length];
        for (int i = 0; i < names.length; i++) {
            names[i] = UseAdvice.items[i].getName();
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_list_item_1,
                names);
        ListView lv = findViewById(R.id.lvUseAvices);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(listUserAdvice.this, AdviceDetail.class);
                intent.putExtra(AdviceDetail.EXTRA_WORKOUT_ID, (int)id);
                startActivity(intent);
            }
        });


    }


}
