package com.nout45.nout45;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

public class ProcessorsAll extends AppCompatActivity implements itemsMenuFragment.Listener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_processors_all);
    }

    @Override
    public void itemClicked(long id) {
        Intent intent = new Intent(this, ProcessorDetail.class);
        intent.putExtra(ComputerDetail.EXTRA_WORKOUT_ID, (int)id);
        startActivity(intent);
    }
}
