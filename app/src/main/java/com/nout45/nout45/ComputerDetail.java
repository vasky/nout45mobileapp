package com.nout45.nout45;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class ComputerDetail extends AppCompatActivity {

    public static final String EXTRA_WORKOUT_ID = "id";
    private int workoutId2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_computer_detail);
    }

    @Override
    public void onStart() {
        super.onStart();

        TextView title = findViewById(R.id.computerName);
        TextView description = findViewById(R.id.descriptionOrder);
        ImageView image = findViewById(R.id.imageOrder);

        int workoutId = (int) getIntent().getExtras().get(EXTRA_WORKOUT_ID);
        Computer workout = Computer.items[(int) workoutId];
        title.setText(workout.getName());
        description.setText(workout.getDescription());
//        Log.i(":",Integer.parseInt( workout.getPhoto()));
        String text = "game_nout";
        image.setImageResource(workout.getRes() );
        setWorkout(workoutId);
    }
    public void setWorkout(int id) {
        this.workoutId2 = id;
    }
}
