package com.nout45.nout45;

public class categoryCatalog {

    public String name;

    public static categoryCatalog[] items = {
            new categoryCatalog("Компьютеры"),
            new categoryCatalog("Процессоры")
    };

    public categoryCatalog(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
}
